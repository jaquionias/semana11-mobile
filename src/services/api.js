import axio from 'axios';

const api = axio.create({
    baseURL: 'http://thebeheroapi.tijack.com.br',
});

export default api;